package com.kirdow.planetcatacombs;

import com.kirdow.planetcatacombs.graphics.Screen;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;

public class Display extends Canvas implements Runnable {

    public static final int WIDTH = 800;
    public static final int HEIGHT = 600;
    public static final String TITLE = "PlanetCatacombs";

    private Thread gameThread;
    private boolean running;

    private BufferedImage framebuffer;
    private int[] pixels;
    private Screen screen;

    private JFrame frame;

    public Display() {
    }

    public void start() {
        if (running) return;
        gameThread = new Thread(this, "PlanetCatacombs");
        running = true;
        gameThread.start();
    }

    public void stop() {
        if (!running) return;
        running = false;
    }

    private void initCanvas() {
        Dimension dimension = new Dimension(WIDTH, HEIGHT);
        this.setSize(dimension);
        this.setPreferredSize(dimension);
        this.setMinimumSize(dimension);
        this.setMaximumSize(dimension);
    }

    private void initDisplay() {
        JFrame frame = new JFrame(TITLE);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.add(this);
        frame.setResizable(false);
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);

        this.frame = frame;
    }

    private void initGraphics() {
        framebuffer = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
        pixels = ((DataBufferInt)framebuffer.getRaster().getDataBuffer()).getData();
        screen = new Screen(WIDTH, HEIGHT).setBuffer(pixels);
    }

    private void initGame() {

    }

    private void init() {
        initCanvas();
        initDisplay();
        initGraphics();;
        initGame();
    }

    public void run() {
        init();

        while (true) {
            tick();
            draw();
        }
    }

    private void tick() {

    }

    private void frame() {
        screen.drawRect(80, 80, 240, 60, 0xFF228811);
    }

    private void draw() {
        BufferStrategy bs = this.getBufferStrategy();
        if (bs == null) {
            this.createBufferStrategy(3);
            this.requestFocus();
            return;
        }

        screen.clear(0xFF555555);

        frame();

        screen.flush();

        Graphics g = bs.getDrawGraphics();
        g.drawImage(framebuffer, 0, 0, WIDTH, HEIGHT, null);
        g.dispose();
        bs.show();
    }


}
