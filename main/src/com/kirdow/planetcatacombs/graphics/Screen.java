package com.kirdow.planetcatacombs.graphics;

import java.util.Arrays;

public class Screen {

    public final int width, height;
    public final int[] pixels;
    private int[] buffer;

    public Screen(int w, int h) {
        this.width = w;
        this.height = h;
        this.pixels = new int[w * h];
        this.buffer = null;
    }

    public Screen setBuffer(int[] buffer) {
        if (buffer == null)
            this.buffer = null;
        else if (buffer.length == pixels.length)
            this.buffer = buffer;
        return this;
    }

    public void flush() {
        if (buffer == null)
            return;

        System.arraycopy(pixels, 0, buffer, 0, pixels.length);
    }

    public void clear(int color) {
        Arrays.fill(pixels, color);
    }

    public void drawRect(int x, int y, int w, int h, int color) {
        for (int y0 = 0; y0 < h; y0++) {
            int y1 = y0 + y;
            if (y1 < 0 || y1 >= height)
                continue;

            int y2 = y1 * width;
            for (int x0 = 0; x0 < w; x0++) {
                int x1 = x0 + x;
                if (x1 < 0 || x1 >= width)
                    continue;

                pixels[x1 + y2] = color;
            }
        }
    }


}
