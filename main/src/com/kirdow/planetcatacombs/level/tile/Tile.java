package com.kirdow.planetcatacombs.level.tile;

public abstract class Tile {

    public final int x, y;
    public final int w, h;

    public Tile(int x, int y, int w, int h) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
    }

    public boolean isSolid() {
        return true;
    }

    public float speedModifier() {
        return 1.0f;
    }

}
